import crypto from 'crypto'

class RSA {
  static async encrypt(data, rsa_key) { // STRING ONLY FOR NOW
    try {
      if (typeof data == "object") data = JSON.stringify(data);
      let chunks = data.match(/.{1,214}/g); // >> (2048/8) - 42 = 214

      let key = rsa_key;

      let enc_obj = {
        key_id: key.id,
        data: new Uint8Array(256*chunks.length)
      }

      for (let c = 0; c < chunks.length; c++) {
        let data_index = c*256;
        let chunk = crypto.publicEncrypt({ key: key.publicKey }, Buffer.from(chunks[c]));
        enc_obj.data.set(chunk, data_index);
      }


//      enc_obj.data = Array.from(enc_obj.data);
//      enc_obj.data = Buffer.from(enc_obj.data).toString("hex");

/*
      let enc_obj = {
        key_id: key.id,
        data: []
      }

      for (let c = 0; c < chunks.length; c++) {
        enc_obj.data.push(crypto.publicEncrypt({ key: key.publicKey }, Buffer.from(chunks[c])));
      }
*/
      return enc_obj.key_id+Buffer.from(enc_obj.data).toString("hex");
    } catch (e) {
      console.error(e.stack);
    }
  }
}

class AES {
  constructor() {
    this.key = crypto.randomBytes(32).toString("base64");
  }

  decrypt(data) {
    const key = Buffer.from(this.key, "base64");

    const iv = Buffer.from(data.substring(0, 24), "base64");
    const auth_tag = Buffer.from(data.substring(24, 48), "base64");
    const encrypted = data.substring(48);

    const decipher = crypto.createDecipheriv('aes-256-gcm', key, iv);
    decipher.setAuthTag(auth_tag);
    let str = decipher.update(encrypted, 'base64', 'utf8');
    str += decipher.final('utf8');
    return str;
  }
}

export {
  RSA,
  AES
}
