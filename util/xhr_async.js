import http from 'https'
//const http = require('http');

import fs from "fs"

export default class {
  static async get(host, port, path, params) {
    try {
      let result = undefined;
      return await new Promise(function(resolve) {

        const req_opts = {
          method: "GET",
          host: host,
          port: port,
          path: path+"?data="+encodeURIComponent(JSON.stringify(params))
        };
        const req_cbak = function(response) {
          var str = '';
          response.on('data', function (chunk) {
            str += chunk;
          });
          response.on('end', function () {
            try {
              result = JSON.parse(str);
            } catch (jsone) {
              result = str;
            }
            resolve(result);
          });
        };

        http.request(req_opts, req_cbak).end();
      });

    } catch (e) {
      console.error(e.stack);
      return result;
    }
  }

  static async get_file(furl, dest_path) {
    try {
      const file = fs.createWriteStream(dest_path);
      await new Promise(function(out) {
        const request = http.get(furl, function(response) {
          response.pipe(file);

          file.on('finish', function() {
            console.log("FF");
            file.close();  // close() is async, call cb after close completes.
          out();
          });

        }).on('error', function(err) { // Handle errors
          fs.unlink(dest_path, function(prm1, prm2) {
            console.log("UNLINK", prm1, prm2);
          }); // Delete the file async. (But we don't check the result)
          console.log(err.message);
          out()
        });;
      });
    } catch (e) {
      console.error(e.stack);
    }
  }

  static async post(host, port, path, params) {
    let result = undefined;
    try {
      var json = "";
      if (typeof params == "string") {
        json = params;
      } else {
        json = JSON.stringify(params);
      }
      var param_str = JSON.stringify({
        data: json
      });

      let extra_headers = {
        'Content-Type': 'application/json',
        'Content-Length': param_str.length,
        'X-Requested-With': 'XMLHttpRequest'
      }

      if (global.authentials) extra_headers.authorization = global.authentials;
      if (global.xss_token) extra_headers.cookie = "user_accounts_access_token="+global.xss_token;

      console.log(extra_headers);

      return await new Promise(function(resolve) {
        var req = http.request({
          host: host,
          path: path,
          port: port,
          method: 'POST',
          headers: extra_headers
        }, function(response) {
          let set_cookies = response.headers["set-cookie"]
          if (set_cookies) {
            for (let c = 0; c < set_cookies.length; c++) {
              let setc = set_cookies[c];
              let cdata = setc.split(" ");
              if (cdata[0].startsWith("user_accounts_access_token=")) {
                global.xss_token = cdata[0].split("=")[1];
              }
            }
          }

          var str = ''
          response.on('data', function (chunk) {
            str += chunk;
          });

          response.on('end', function () {
            try {
              result = JSON.parse(str);
            } catch (jsone) {
              result = str;
            }
            resolve(result);
          });
        });
        req.write(param_str);
        req.end();
      });
    } catch (e) {
      console.error(e.stack);
      return result;
    }
  }
}
