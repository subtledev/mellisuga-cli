
import path from "path"

import os from "os"

let data_dir = path.resolve(os.homedir(), ".mellisuga");

export default class {
  static async run() {
    try {
      let script_name = process.argv[3];



      let script_path = path.resolve(data_dir, "scripts", script_name, "index.js");
      let script_class = await import(script_path);
      console.log(script_class.default);
      await script_class.default.run();

    } catch (e) {
      console.error(e.stack);
    }
  }
}
